from faker import Faker
from faker_sqlalchemy import SqlAlchemyProvider
from sqlalchemy import insert

from config.database import sessionLocal
from config import models

fake = Faker('it_IT')
Session = sessionLocal
# session = Session()

fake.add_provider(SqlAlchemyProvider)


def custom_seed_file_server(n, db=sessionLocal):
    session = db()
    session.execute(
        insert(models.FileServer),
        [
            {
                "file": fake.name(),
            }
            for i in range(n)
        ],
    )
    session.flush()
    session.commit()
    session.close()


def custom_seed_user(n, db=sessionLocal):
    session = db()
    session.execute(
        insert(models.Users),
        [
            {
                "first_name": fake.first_name(),
                "last_name": fake.last_name(),
                "email": fake.email(),
                "password": fake.password(),
                "profile_picture_id": i + 1
            }
            for i in range(n)
        ],
    )

    session.flush()
    session.commit()
    session.close()


def custom_seed_category(n, db=sessionLocal):
    session = db()
    session.execute(
        insert(models.Category),
        [
            {
                "title": fake.company(),
            }
            for i in range(n)
        ],
    )

    session.flush()
    session.commit()
    session.close()


def custom_seed_blog(n, db=sessionLocal):
    session = db()
    session.execute(
        insert(models.Blogs),
        [
            {
                "blog_title": fake.company(),
                "blog_body": fake.text(200),
                "category_id": i + 1,
                "author_id": i + 1
            }
            for i in range(n)
        ],
    )

    session.flush()
    session.commit()
    session.close()


def custom_seed_blog_image(n, db=sessionLocal):
    session = db()
    session.execute(
        insert(models.BlogImage),
        [
            {
                "blog_id": fake.random_int(1, n),
                "image_id": fake.random_int(1, n),
            }
            for i in range(n)
        ],
    )

    session.flush()
    session.commit()
    session.close()


def custom_seed(n=10, db=sessionLocal):
    custom_seed_category(n, db)
    custom_seed_file_server(n, db)
    custom_seed_user(n, db)
    custom_seed_blog(n, db)
    custom_seed_blog_image(n, db)


if __name__ == "__main__":
    custom_seed(20)

#
# def auto_seed(n, model=models.FileServer, db=session):
#     db.add_all(
#         [
#             fake.sqlalchemy_model(model)
#             for i in range(n)
#         ]
#     )
#     db.flush()
#     db.commit()
#
#
# def custom_seed_pk_given(n, db=session):
#     """Batched INSERT statements via the ORM, PKs already defined"""
#     for chunk in range(0, n, 1000):
#         db.add_all(
#             [
#                 Test(
#                     id=i + 1,
#                     name="customer name %d" % i,
#                 )
#                 for i in range(chunk, chunk + 1000)
#             ]
#         )
#         db.flush()
#     db.commit()
#
#
# def custom_seed_insert_returning(n, db=session):
#     """Batched INSERT statements via the ORM in "bulk", returning new Customer
#     objects"""
#
#     customer_result = db.scalars(
#         insert(Test).returning(Test),
#         [
#             {
#                 "name": "customer name %d" % i,
#             }
#             for i in range(n)
#         ],
#     )
#
#     # this step is where the rows actually become objects
#     customers = customer_result.all()  # noqa: F841
#
#     db.commit()
