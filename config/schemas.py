from pydantic import BaseModel


class BlogBase(BaseModel):
    blog_title: str
    blog_body: str
    category_id: int
    author_id: int
