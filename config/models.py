from typing import List, Optional

from sqlalchemy import String, DateTime, Column, ForeignKey, Text
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from config.database import Base


# association table for many to many relation between blogs and file_server
class BlogImage(Base):
    __tablename__ = "blog_images"
    blog_id: Mapped[int] = mapped_column(ForeignKey("blogs.id"), primary_key=True)
    image_id: Mapped[int] = mapped_column(ForeignKey("file_server.id"), primary_key=True)

    blog: Mapped["Blogs"] = relationship(back_populates="image_association")
    image: Mapped["FileServer"] = relationship(back_populates="blog_association")


class TimeStamp:
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class Users(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    first_name = Column(String(25))
    last_name = Column(String(25))
    email = Column(Text)
    password = Column(Text)
    profile_picture_id: Mapped[Optional[int]] = mapped_column(
        ForeignKey("file_server.id"))  # one-to-one relation with file_server
    blog: Mapped[List["Blogs"]] = relationship(back_populates="user")  # for one-to-many relation with blogs table
    profile_picture: Mapped["FileServer"] = relationship(back_populates="user")


class Blogs(Base, TimeStamp):
    __tablename__ = "blogs"
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    blog_title = Column(Text)
    blog_body = Column(Text)
    category_id: Mapped[int] = mapped_column(ForeignKey("blog_category.id"))
    author_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    user: Mapped["Users"] = relationship(back_populates="blog")  # for one-to-many relation with users table
    category: Mapped["Category"] = relationship(back_populates="blog")
    images: Mapped[List["FileServer"]] = relationship(secondary='blog_images', back_populates="blogs")
    image_association: Mapped[List["BlogImage"]] = relationship(back_populates="blog")


class FileServer(Base):
    __tablename__ = "file_server"
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    file = Column(Text)
    user: Mapped["Users"] = relationship(back_populates="profile_picture")
    blogs: Mapped["Blogs"] = relationship(secondary='blog_images', back_populates="images")
    blog_association: Mapped[List["BlogImage"]] = relationship(back_populates="image")


class Category(Base):
    __tablename__ = "blog_category"
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    title = Column(Text)
    blog: Mapped[List["Blogs"]] = relationship(back_populates='category')

# class Test(Base):
#     __tablename__ = 'test'
#     id = Column(Integer, primary_key=True)
#     title = Column(Text)
