from fastapi import APIRouter, Depends, HTTPException

router = APIRouter(
    prefix="/dashboard",
    # dependencies=[Depends(get_token_header)],
)


@router.get('/ping')
def ping():
    return 'pong'


@router.get('/pong')
def ping():
    return 'pong'
