from fastapi import APIRouter
from router.api import dashboard_controller

router = APIRouter(
    prefix="/api",
    tags=["api"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)

router.include_router(dashboard_controller.router, prefix='/dashboard')
